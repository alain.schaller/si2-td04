<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="ch.hefr.si2.td04.springmvc1.service.StudentsService" %>
<%@ page import="ch.hefr.si2.td04.springmvc1.model.Student" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello</title>
</head>
<body>
    <%--<%--%>
        <%--for (Student student : service.getAllStudents()) {--%>
            <%--out.println("<div>" + student.getName() + "</div>");--%>
        <%--}--%>
    <%--%>--%>

    <% StudentsService service = (StudentsService) request.getAttribute("service"); %>
    <c:forEach var="student" items="${service.getAllStudents()}" >
        <div><c:out value="${student.getName()}" /></div>
    </c:forEach>

    <form action="/students" method="post">
        <input type="text" name="name" placeholder="Alain">
        <input type="submit" title="add student">
    </form>
</body>
</html>