package ch.hefr.si2.td04.springmvc1.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findAll();
    Optional<Student> findById(Long l);
    List<Student> findAllByOrderByNameAsc();
}
