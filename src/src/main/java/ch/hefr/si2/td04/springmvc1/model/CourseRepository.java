package ch.hefr.si2.td04.springmvc1.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends CrudRepository<Course, Long> {
    List<Course> findAll();
    Optional<Course> findById(Long l);
}
