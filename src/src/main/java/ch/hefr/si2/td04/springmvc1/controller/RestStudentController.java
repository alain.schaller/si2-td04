package ch.hefr.si2.td04.springmvc1.controller;

import ch.hefr.si2.td04.springmvc1.exceptions.StudentNotFoundException;
import ch.hefr.si2.td04.springmvc1.model.Student;
import ch.hefr.si2.td04.springmvc1.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestStudentController {

    private final StudentsService studentsService;

    @Autowired
    public RestStudentController(StudentsService studentsService) {
        this.studentsService = studentsService;
    }

    @GetMapping("/api/students")
    public List<Student> all() {
        return this.studentsService.getAllStudents();
    }

    @PostMapping("/api/students")
    public Student post(@RequestParam(value = "name") String newStudentName) {
        return this.studentsService.postStudent(new Student(newStudentName));
    }

    @GetMapping("/api/students/{id}")
    public Student get (@PathVariable Long id) {
        return this.studentsService.getStudent(id).orElseThrow(() -> new StudentNotFoundException(id));
    }

    @PutMapping("/api/students/{id}")
    public Student put (@PathVariable Long id, @RequestParam(value = "name") String newStudentName) {
        Student student = get(id);
        student.setName(newStudentName);
        this.studentsService.putStudent(student);

        return student;
    }

    @DeleteMapping("/api/students/{id}")
    public ResponseEntity<Student> deleteStudent (@PathVariable Long id) {
        return this.studentsService.deleteStudent(id);
    }
}
