package ch.hefr.si2.td04.springmvc1.service;

import ch.hefr.si2.td04.springmvc1.model.Course;
import ch.hefr.si2.td04.springmvc1.model.CourseRepository;
import ch.hefr.si2.td04.springmvc1.model.Student;
import ch.hefr.si2.td04.springmvc1.model.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CoursesService {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private StudentsService studentsService;

    public List<Course> getAllCourses () {
        return this.courseRepository.findAll();
    }

    public Optional<Course> getCourse(Long c) {
        return this.courseRepository.findById(c);
    }

    public Course postCourse(Course c) {
        return this.courseRepository.save(c);
    }

    public Course putCourse(Course c) {
        return this.courseRepository.save(c);
    }

    public ResponseEntity<Course> deleteCourse(Long c) {
        try {
            this.courseRepository.deleteById(c);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public Optional<Course> addStudent (Long courseId, Long studentId) {
        Optional<Course> optionalCourse = this.courseRepository.findById(courseId);
        if ( ! optionalCourse.isPresent())
            return Optional.empty();

        Optional<Student> optionalStudent = this.studentsService.getStudent(studentId);
        if ( ! optionalStudent.isPresent())
            return Optional.empty();

        Course course = optionalCourse.get();
        Student student = optionalStudent.get();
        course.addStudent(student);
        student.joinCourse(course);
        this.courseRepository.save(course);

        return optionalCourse;
    }
}
