package ch.hefr.si2.td04.springmvc1.service;

import ch.hefr.si2.td04.springmvc1.model.Student;
import ch.hefr.si2.td04.springmvc1.model.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class StudentsService {

    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getAllStudents () {
        return this.studentRepository.findAllByOrderByNameAsc();
    }

    public Optional<Student> getStudent(Long s) {
        return this.studentRepository.findById(s);
    }

    public Student postStudent(Student s) {
        return this.studentRepository.save(s);
    }

    public Student putStudent(Student s) {
        return this.studentRepository.save(s);
    }

    public ResponseEntity<Student> deleteStudent(Long s) {
        try {
            this.studentRepository.deleteById(s);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
