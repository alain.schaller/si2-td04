package ch.hefr.si2.td04.springmvc1.controller;

import ch.hefr.si2.td04.springmvc1.exceptions.CourseNotFoundException;
import ch.hefr.si2.td04.springmvc1.exceptions.NotFoundException;
import ch.hefr.si2.td04.springmvc1.model.Course;
import ch.hefr.si2.td04.springmvc1.model.Student;
import ch.hefr.si2.td04.springmvc1.service.CoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestCourseController {

    private final CoursesService coursesService;

    @Autowired
    public RestCourseController(CoursesService coursesService) {
        this.coursesService = coursesService;
    }

    /*   ---------------- REST API ----------------   */

    @GetMapping("/api/courses")
    public List<Course> all() {
        return this.coursesService.getAllCourses();
    }

    @PostMapping("/api/courses")
    public Course post(@RequestParam(value = "name") String newCourseName) {
        return this.coursesService.postCourse(new Course(newCourseName));
    }

    @GetMapping("/api/courses/{id}")
    public Course get (@PathVariable Long id) {
        return this.coursesService.getCourse(id).orElseThrow(() -> new CourseNotFoundException(id));
    }

    @PutMapping("/api/courses/{id}")
    public Course put (@PathVariable Long id, @RequestParam(value = "name") String newCourseName) {
        Course student = get(id);
        student.setName(newCourseName);
        this.coursesService.putCourse(student);

        return student;
    }

    @DeleteMapping("/api/courses/{id}")
    public ResponseEntity<Course> deleteCourse (@PathVariable Long id) {
        return this.coursesService.deleteCourse(id);
    }

    /*   ---------------- COURSE'S STUDENTS ----------------   */

    @GetMapping("/api/courses/{id}/students")
    public List<Student> getCourseStudents (@PathVariable Long id) {
        return this.get(id).getStudents();
    }

    @PutMapping("/api/courses/{id}/students/{studentId}")
    public Course addStudentToCourse (@PathVariable Long id, @PathVariable Long studentId) {
        return this.coursesService.addStudent(id, studentId).orElseThrow(NotFoundException::new);
    }
}
