package ch.hefr.si2.td04.springmvc1.controller;

import ch.hefr.si2.td04.springmvc1.model.Student;
import ch.hefr.si2.td04.springmvc1.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

@Controller
public class StudentsController {

    private final StudentsService studentsService;

    @Autowired
    public StudentsController (StudentsService studentsService) {
        this.studentsService = studentsService;
    }

    @GetMapping("/students") public String students(
            Model model//,
            //@RequestParam(value = "name", required = false, defaultValue = "World") String name
    ) {
        model.addAttribute("service", this.studentsService);
        return "students";
    }

    @PostMapping("/students") public String addStudent(Model model, @RequestParam(value = "name") String newStudentName) {
        this.studentsService.postStudent(new Student(newStudentName));
        return this.students(model);
    }
}
