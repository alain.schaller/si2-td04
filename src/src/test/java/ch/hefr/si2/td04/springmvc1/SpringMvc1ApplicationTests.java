package ch.hefr.si2.td04.springmvc1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringMvc1ApplicationTests {

    @Test
    public void contextLoads() {
    }

}
